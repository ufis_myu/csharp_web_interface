﻿using Microsoft.Practices.Prism.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using log4net;

namespace Ufis.WebBrowser
{
    public partial class MyBootstrapper
    {
        private readonly Log4NetLogger _logger = new Log4NetLogger();

        protected override ILoggerFacade CreateLogger()
        {
            return _logger;
        }

        protected override DependencyObject CreateShell()
        {
            DependencyObject shell = null;

            Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;

            string systemPath = Environment.GetEnvironmentVariable("UFISSYSTEM");
            if (!string.IsNullOrEmpty(systemPath))
            {
                string filePath = string.Format("{0}\\{1}", systemPath, "Ceda.ini");
                if (File.Exists(filePath))
                {
                    var myIni = new IniFile(filePath);
                    if (myIni.IniReadValue("GLOBAL", "MANPOWER_GAP_HANDLE_URL") == "N")
                    {
                        // error
                    }
                    else
                    {
                        var val = myIni.IniReadValue("GLOBAL", "MANPOWER_GAP_HANDLE_URL");
                        Console.WriteLine(val);
                    }
                }
            }
            shell = this.Container.GetExportedValue<Shell>();
            Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;

            if (shell == null)
                Application.Current.Shutdown();

            return shell;
        }
    }
}
