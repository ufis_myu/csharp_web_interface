﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ufis.WebBrowser
{
    public class AppArguments
    {
        private static AppArguments _this;

        public static AppArguments Current
        {
            get
            {
                if (_this == null)
                {
                    _this = new AppArguments();
                }

                return _this;
            }
        }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Destination { get; set; }
    }
}
