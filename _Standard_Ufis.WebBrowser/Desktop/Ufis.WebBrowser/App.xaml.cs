﻿using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Ufis.WebBrowser
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            DispatcherUnhandledException += (o, e) => HandleException(e.Exception);
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            //LoadResourceDictionaries(this);

            ConfigureLogger("StandardLog.config");

#if (DEBUG)
            RunInDebugMode();
#else
            RunInReleaseMode();
#endif
        }

        private static void LoadResourceDictionaries(Application app)
        {
            var res = new ResourceDictionary()
            {
                Source = new Uri("/Ufis.Resources;component/XAML/DefaultStyles.xaml", UriKind.Relative)
            };
            app.Resources.MergedDictionaries.Add(res);
        }

        private static void ConfigureLogger(string path)
        {
            if (System.IO.File.Exists(path))
            {
                var fileInfo = new System.IO.FileInfo(path);
                log4net.Config.XmlConfigurator.ConfigureAndWatch(fileInfo);
            }
        }

        private static void RunInDebugMode()
        {
            var bootstrapper = new MyBootstrapper();
            bootstrapper.Run();
        }

        private static void RunInReleaseMode()
        {
            AppDomain.CurrentDomain.UnhandledException += AppDomainUnhandledException;
            try
            {
                var bootstrapper = new MyBootstrapper();
                bootstrapper.Run();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private static void AppDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            HandleException(e.ExceptionObject as Exception, true);
        }

        private static void HandleException(Exception ex)
        {
            HandleException(ex, false);
        }

        private static void HandleException(Exception ex, bool setExceptionPolicy)
        {
            if (ex == null)
            {
                return;
            }
            if (setExceptionPolicy)
            {
                ExceptionPolicy.HandleException(ex, "Default Policy");
            }

            //DisposeBroadcast();

            //Ufis.Utilities.CommonUtils.ShowExceptionDialog(ex);

            //exit
            Environment.Exit(1);
        }
    }
}
