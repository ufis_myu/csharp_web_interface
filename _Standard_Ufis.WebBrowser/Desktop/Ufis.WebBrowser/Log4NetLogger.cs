﻿using log4net;
using Microsoft.Practices.Prism.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ufis.WebBrowser
{
    public class Log4NetLogger : ILoggerFacade
    {
        #region Fields

        // Member variables
        private readonly ILog _logger = LogManager.GetLogger(typeof(Log4NetLogger));

        #endregion

        #region ILoggerFacade Members

        /// <summary>
        /// Write a new log entry with the specified category and priority.
        /// </summary>
        /// <param name="message">Message body to log.</param>
        /// <param name="category">Category of the entry.</param>
        /// <param name="priority">The priority of the entry.</param>
        public void Log(string message, Category category, Priority priority)
        {
            switch (category)
            {
                case Category.Debug:
                    _logger.Debug(message);
                    break;
                case Category.Warn:
                    _logger.Warn(message);
                    break;
                case Category.Exception:
                    _logger.Error(message);
                    break;
                case Category.Info:
                    _logger.Info(message);
                    break;
            }
        }

        #endregion
    }
}
