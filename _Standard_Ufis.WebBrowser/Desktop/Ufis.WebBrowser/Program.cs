﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ufis.WebBrowser;

namespace Ufis.Dashboard
{
    static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            ParseArguments(args);

            App app = new App();
            app.Run();
        }

        private static void ParseArguments(string[] args)
        {
            AppArguments arguments = AppArguments.Current;

            if (args != null && args.Length > 0)
            {
                string[] arrArgs = args[0].Split('|');

                if (arrArgs[0] != null) arguments.UserName = arrArgs[0];

                if (arrArgs[1] != null) arguments.Password = arrArgs[1];

                if (arrArgs[2] != null) arguments.Destination = arrArgs[2];
            }
        }
    }
}
