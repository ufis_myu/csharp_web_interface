﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Ufis.Core;
using Ufis.Utilities;

namespace Ufis.Dashboard
{
    [Export(typeof(IApplication))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class CurrentApplication : IApplication
    {
        private const string PRODUCT_CODE = "WEBBROWSER";
        private const string PRODUCT_NAME = "Web Browser";
        private const string DESCRIPTION = "Web Browser";

        public string ProductCode
        {
            get
            {
                return PRODUCT_CODE;
            }
        }

        public string ProductName
        {
            get
            {
                return PRODUCT_NAME;
            }
        }

        public string Version
        {
            get
            {
                return CommonUtils.GetApplicationVersion(Assembly.GetExecutingAssembly());
            }
        }

        public ImageSource Glyph
        {
            get
            {
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri("pack://application:,,,/Ufis.RMS.Rules;component/app_icon.ico");
                bitmap.EndInit();
                return bitmap;
            }
        }

        public ImageSource LargeGlyph
        {
            get
            {
                return CommonUtils.GetImage("clock_32x32.png");
            }
        }

        public Geometry VectorImage
        {
            get
            {
                return System.Windows.Application.Current.TryFindResource("VectorDelay") as Geometry;
            }
        }

        public string Description
        {
            get
            {
                return DESCRIPTION;
            }
        }
    }
}
