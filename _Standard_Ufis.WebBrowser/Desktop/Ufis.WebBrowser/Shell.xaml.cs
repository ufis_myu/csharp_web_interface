﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ufis.WebBrowser
{
    /// <summary>
    /// Interaction logic for Shell.xaml
    /// </summary>
    [Export]
    public partial class Shell : MetroWindow
    {
        public Shell()
        {
            InitializeComponent();

            webBrowser.Address = this.LoadAddress();
        }

        private string LoadAddress()
        {
            string address = string.Empty;
            string systemPath = Environment.GetEnvironmentVariable("UFISSYSTEM");
            if (!string.IsNullOrEmpty(systemPath))
            {
                string filePath = string.Format("{0}\\{1}", systemPath, "Ceda.ini");
                if (File.Exists(filePath))
                {
                    var myIni = new IniFile(filePath);
                    string strUrl = myIni.IniReadValue("GLOBAL", "MANPOWER_GAP_HANDLE_URL");
                    if (!string.IsNullOrEmpty(strUrl))
                    {
                        address = strUrl;
                        string dest = string.Empty;

                        if (!string.IsNullOrEmpty(AppArguments.Current.Destination))
                        {
                            if (AppArguments.Current.Destination == "APPSCR") dest = "REQAPP";
                        }

                        address = string.Format("{0}#/v_officer_login/{1}/{2}/{3}",
                                strUrl,
                                AppArguments.Current.UserName,
                                AppArguments.Current.Password,
                                dest);
                    }
                }
            }

            return address;
        }
    }
}
