﻿using Microsoft.Practices.Prism.MefExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;

namespace Ufis.WebBrowser
{
    public partial class MyBootstrapper : MefBootstrapper
    {
        protected override void ConfigureAggregateCatalog()
        {
            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly()));
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
        }

        protected override void InitializeShell()
        {
            base.InitializeShell();

#if SILVERLIGHT
                Application.Current.RootVisual = (Shell)this.Shell;            
#else
            Application.Current.MainWindow = (Window)this.Shell;
            Application.Current.MainWindow.Show();
#endif
        }
    }
}
