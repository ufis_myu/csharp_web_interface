﻿using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Net;
using System.Text;
using Ufis.Core;
using Ufis.Data;
using Ufis.Utilities;

namespace Ufis.Dashboard
{
    [Export(typeof(IDataCentre))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class DataCentre : DataCentreBase
    {
        [ImportingConstructor]
        public DataCentre(IDataFactory dataFactory)
            : base(dataFactory)
        {

        }

        protected override IDataCommand CreateSelectCommand(Type entityType, string attributeList, string whereClause)
        {
            var command = ServiceLocator.Current.GetInstance<EntitySelectCommand>();
            command.EntityType = entityType;
            command.AttributeList = attributeList;
            command.EntityWhereClause = whereClause;

            return command;
        }

        protected override int GetWindowsCodePage()
        {
            int windowsCodePage;
            if (int.TryParse(CommonUtils.GetWindowsCodePage(), out windowsCodePage))
                return windowsCodePage;
            else
                return -1;
        }

        protected override string GetWorkstationName()
        {
#if SILVERLIGHT
            return string.Empty; //todo
#else
            return Environment.MachineName;
#endif
        }

        protected override string GetWorkstationIPAddress()
        {

#if SILVERLIGHT
            return string.Empty; //todo
#else
            var ipHostEntry = Dns.GetHostEntry(GetWorkstationName());
            if (ipHostEntry == null)
            {
                return null;
            }
            else
            {
                var ipAddress = ipHostEntry.AddressList.FirstOrDefault(ip =>
                    ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
                if (ipAddress == null)
                    return null;
                else
                    return ipAddress.ToString();
            }
#endif
        }
    }
}
